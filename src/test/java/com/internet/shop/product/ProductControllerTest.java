package com.internet.shop.product;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductRepository productRepository;

    @Test
    @Transactional
    public void whenCreateProduct() throws Exception {
        Product productEntity = new Product("Some trash product", "Super total reducer X-2000", 3, 299.99);
        Product testProduct = createTestProductPersist(productEntity);

        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/products/create")
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .param("title", testProduct.getTitle())
                        .param("price", testProduct.getPrice() + "")
                        .param("description", testProduct.getDescription())
                        .param("count", testProduct.getQuantityInStock() + "")
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.title", Matchers.is(testProduct.getTitle())))
                .andExpect(jsonPath("$.price", Matchers.is(testProduct.getPrice())))
                .andExpect(jsonPath("$.description", Matchers.is(testProduct.getDescription())))
                .andExpect(jsonPath("$.quantityInStock", Matchers.is(testProduct.getQuantityInStock())))
                .andDo(print())
        ;
        productRepository.delete(testProduct);
    }

    @Test
    @Transactional
    public void whenGetProductById() throws Exception {
        Product productEntity = new Product("Some trash product", "Super total reducer X-2000", 3, 299.99);
        Product testProduct = createTestProductPersist(productEntity);

        this.mockMvc.perform(
                MockMvcRequestBuilders.get("/products/" + testProduct.getId())
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON)

        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.id", Matchers.is(testProduct.getId())))
                .andExpect(jsonPath("$.title", Matchers.is(testProduct.getTitle())))
                .andExpect(jsonPath("$.price", Matchers.is(testProduct.getPrice())))
                .andExpect(jsonPath("$.description", Matchers.is(testProduct.getDescription())))
                .andExpect(jsonPath("$.quantityInStock", Matchers.is(testProduct.getQuantityInStock())))
                .andDo(print())
        ;
        productRepository.delete(testProduct);
    }

    private Product createTestProductPersist(Product product) {
        return productRepository.save(product);
    }

}