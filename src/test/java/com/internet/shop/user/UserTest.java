package com.internet.shop.user;

import com.internet.shop.country.Country;
import com.internet.shop.credit.card.CreditCard;
import com.internet.shop.product.Product;
import com.internet.shop.product.ProductRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.Set;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    void contextLoads() {
        Assert.assertNotNull(userRepository);
        Assert.assertNotNull(productRepository);
    }

    @Test
    @Transactional
    public void whenCreateUser() throws Exception {
        User user = new User("Vasya", "petya-vasya@gmail.lol", new Country("Ukraine"));

        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/users/create")
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .param("userName", user.getUserName())
                        .param("email", user.getEmail())
                        .param("country", user.getCountry().getTitle())
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.userName", Matchers.is(user.getUserName())))
                .andExpect(jsonPath("$.email", Matchers.is(user.getEmail())))
        ;
        userRepository.delete(user);
    }

    @Test
    @Transactional
    public void whenAddCreditCardToUserAccount() throws Exception {
        User testUserEntity = new User("Vasya", "petya-vasya@gmail.lol", new Country("Ukraine"));
        User testUser = createTestUserPersist(testUserEntity);

        CreditCard usersCreditCard = new CreditCard(testUser, 1234123412341234L);

        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/users/" + testUser.getId() + "/add-credit-card")
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("primaryAccountNumber", usersCreditCard.getPrimaryAccountNumber() + "")
        )
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.primaryAccountNumber", Matchers.is(usersCreditCard.getPrimaryAccountNumber())))
        ;
        userRepository.delete(testUser);
    }

    @Test
    @Transactional
    public void whenFindUserById() throws Exception {
        User testUserEntity = new User("Vasya", "petya-vasya@gmail.lol", new Country("Ukraine"), new CreditCard(1111222233334444L));
        User testUser = createTestUserPersist(testUserEntity);

        Set<CreditCard> userCreditCards = testUser.getCreditCards();
        Iterator iter = userCreditCards.iterator();
        CreditCard firstCreditCard = (CreditCard) iter.next();

        this.mockMvc.perform(
                MockMvcRequestBuilders.get("/users/" + testUser.getId())
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON)

        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(testUser.getId())))
                .andExpect(jsonPath("$.userName", Matchers.is(testUser.getUserName())))
                .andExpect(jsonPath("$.email", Matchers.is(testUser.getEmail())))
                .andExpect(jsonPath("$.registrationDate").exists())
                .andExpect(jsonPath("$.country.id").exists())
                .andExpect(jsonPath("$.country.title", Matchers.is(testUser.getCountry().getTitle())))
                .andExpect(jsonPath("$.creditCards[0].id").exists())
                .andExpect(jsonPath("$.creditCards[0].primaryAccountNumber", Matchers.is(firstCreditCard.getPrimaryAccountNumber())))
                .andExpect(jsonPath("$.cartItems").exists())
                .andExpect(jsonPath("$.orderList").exists())
        ;
        userRepository.delete(testUser);
    }

    @Test
    @Transactional
    public void whenAddProductToCart() throws Exception {
        Product productEntity = new Product("Some trash product", "Super total reducer X-2000", 3, 299.99);
        Product testProduct = createTestProductPersist(productEntity);

        User testUserEntity = new User("Vasya", "petya-vasya@gmail.lol", new Country("Ukraine"));
        User testUser = createTestUserPersist(testUserEntity);

        int countProductInCart = 2;

        this.mockMvc.perform(
                MockMvcRequestBuilders.post(
                        "/users/" + testUser.getId() + "/cart/add-product/" + testProduct.getId()
                )
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("count", countProductInCart + "")
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.product.id").exists())
                .andExpect(jsonPath("$.product.title", Matchers.is(testProduct.getTitle())))
                .andExpect(jsonPath("$.product.description", Matchers.is(testProduct.getDescription())))
                .andExpect(jsonPath("$.product.quantityInStock", Matchers.is(testProduct.getQuantityInStock())))
                .andExpect(jsonPath("$.product.price", Matchers.is(testProduct.getPrice())))
                .andExpect(jsonPath("$.count", Matchers.is(countProductInCart)))

        ;
        productRepository.delete(testProduct);
        userRepository.delete(testUser);
    }

    @Test
    @Transactional
    @Sql(value = {"/test/before-test-buy-all-items-from-cart.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/test/after-test-buy-all-items-from-cart.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void buyAllItemsFromCart() throws Exception {
        int itemsCountForBuy = 1;

        this.mockMvc.perform(
                MockMvcRequestBuilders.post(
                        "/users/" + 0 + "/cart/buy-all-items"
                )
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .accept(MediaType.APPLICATION_JSON)
                        .param("count", String.valueOf(itemsCountForBuy))
        )
                .andExpect(status().isOk())
                .andDo(print())
        ;
    }

    private User createTestUserPersist(User user) {
        return userRepository.save(user);
    }

    private Product createTestProductPersist(Product product) {
        return productRepository.save(product);
    }
}
