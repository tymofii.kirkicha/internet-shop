package com.internet.shop.credit.card;

public class CreditCardDTO {
    private Integer id;
    private Long primaryAccountNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    public void setPrimaryAccountNumber(Long primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }
}
