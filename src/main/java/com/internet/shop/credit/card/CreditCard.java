package com.internet.shop.credit.card;

import com.internet.shop.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true, length = 16)
    @NotNull
    private Long primaryAccountNumber;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private User user;

    public CreditCard() {
    }

    public CreditCard(User user, Long primaryAccountNumber) {
        this.user = user;
        this.primaryAccountNumber = primaryAccountNumber;
    }

    public CreditCard(long primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    public void setPrimaryAccountNumber(Long primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
