package com.internet.shop.credit.card;

import com.internet.shop.user.User;
import com.internet.shop.user.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CreditCardService {

    private CreditCardRepository creditCardRepository;
    private UserRepository userRepository;

    public CreditCardService(CreditCardRepository creditCardRepository, UserRepository userRepository) {
        this.creditCardRepository = creditCardRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public CreditCard createCreditCard(Integer userId, Long primaryAccountNumber) {
        User user;
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        } else {
            return null;
        }
        CreditCard creditCard = new CreditCard(user, primaryAccountNumber);
        return creditCardRepository.save(creditCard);
    }
}
