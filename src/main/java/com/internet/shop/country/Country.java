package com.internet.shop.country;

import com.internet.shop.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String title;

    @OneToOne(fetch = FetchType.LAZY)
    private User user;

    public Country(String country, User user) {
        this.title = country;
        this.user = user;
    }

    public Country() {
    }

    public Country(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
