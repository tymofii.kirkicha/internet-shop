package com.internet.shop.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Query(value = "SELECT * FROM `product` as p WHERE p.title = ?1 LIMIT 1;",
            nativeQuery = true
    )
    Optional<Product> findByTitle(String title);
}
