package com.internet.shop.product;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    private ProductService productService;
    private final ModelMapper modelMapper;

    public ProductController(ProductService productService, ModelMapper modelMapper) {
        this.productService = productService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(value = "/create")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ProductDTO create(@RequestParam String title,
                             @RequestParam double price,
                             @RequestParam(required = false) String description,
                             @RequestParam Integer count) {
        Product product = productService.create(title, price, description, count);
        return modelMapper.map(product, ProductDTO.class);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getProductById(@PathVariable Integer id) {
        Optional<Product> optionalProduct = productService.getProductById(id);
        Product product;
        if (optionalProduct.isPresent()) {
            product = optionalProduct.get();
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
        return ResponseEntity.ok(productDTO);
    }
}
