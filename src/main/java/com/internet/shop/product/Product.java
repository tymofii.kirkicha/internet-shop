package com.internet.shop.product;

import com.internet.shop.cart.item.CartItem;
import com.internet.shop.orders.Orders;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Column(unique = true)
    private String title;

    private String description;

    @NotNull
    private Integer quantityInStock;

    @NotNull
    private double price;

    @NotNull
    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
    private Set<CartItem> cartItems = new HashSet<>();

    @NotNull
    @OneToMany(targetEntity = Orders.class, mappedBy = "product", fetch = FetchType.LAZY)
    private Set<Orders> orders = new HashSet<>();

    Product(@NotNull String title, @NotNull double price, @NotNull Integer quantityInStock) {
        this.title = title;
        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public Product(@NotNull String title, String description, @NotNull Integer quantityInStock, @NotNull double price) {
        this.title = title;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.price = price;
    }

    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(Integer quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Set<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Set<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public Set<Orders> getOrders() {
        return orders;
    }

    public void setOrders(Set<Orders> orders) {
        this.orders = orders;
    }
}
