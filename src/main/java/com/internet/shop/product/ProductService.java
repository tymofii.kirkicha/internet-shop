package com.internet.shop.product;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional
    Product create(String title, double price, String description, Integer count) {
        Product product = new Product(title, price, count);
        if (description != null) {
            product.setDescription(description);
        }
        return productRepository.save(product);
    }

    @Transactional(readOnly = true)
    Optional<Product> getProductById(Integer id) {
        return productRepository.findById(id);
    }
}
