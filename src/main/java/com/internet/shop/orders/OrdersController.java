package com.internet.shop.orders;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/orders")
public class OrdersController {

    private OrdersService ordersService;
    private final ModelMapper modelMapper;

    public OrdersController(OrdersService ordersService, ModelMapper modelMapper) {
        this.ordersService = ordersService;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/reports/last-week-purchases")
    public Set<OrdersDTO> getAllOrdersOfProductByUserInLastWeek(@RequestParam Integer userId,
                                                                @RequestParam Integer productId) {
        Set<Orders> orders = ordersService.getAllOrdersOfProductByUserInLastWeek(userId, productId);
        return orders.stream()
                .map(o -> modelMapper.map(o, OrdersDTO.class))
                .collect(Collectors.toSet());
    }

    @GetMapping(value = "/reports/sum-and-count-purchases-of-product-by-date")
    public SumAndCountPurchasesDTO getNumberPurchasesOfProductByDate(@RequestParam Integer productId,
                                                                     @RequestParam String date) {
        return ordersService.getSumAndCountPurchasesOfProductByDate(productId, date);
    }

    @GetMapping(value = "/reports/all-purchases-for-period-group-by-user-region")
    private Set<OrdersDTO> getAllPurchasesForPeriodGroupByUserRegion(@RequestParam String fromDate,
                                                                     @RequestParam String toDate) {
        Set<Orders> orders = ordersService.getAllPurchasesForPeriodGroupByUserRegion(fromDate, toDate);
        return orders.stream()
                .map(o -> modelMapper.map(o, OrdersDTO.class))
                .collect(Collectors.toSet());
    }
}
