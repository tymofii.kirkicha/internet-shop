package com.internet.shop.orders;

import com.internet.shop.product.ProductDTO;

import java.time.LocalDateTime;

public class OrdersDTO {
    private Integer id;
    private Integer quantity = 1;
    private LocalDateTime orderDateTime;
    private ProductDTO product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public LocalDateTime getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(LocalDateTime orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }
}
