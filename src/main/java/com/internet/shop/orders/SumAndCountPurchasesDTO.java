package com.internet.shop.orders;

import java.util.Objects;

public class SumAndCountPurchasesDTO {
    private Integer sumOfPurchases;
    private Integer countOfPurchases;

    SumAndCountPurchasesDTO(Integer sumOfPurchases, Integer countOfPurchases) {
        this.sumOfPurchases = Objects.requireNonNullElse(sumOfPurchases, 0);
        this.countOfPurchases = countOfPurchases;
    }

    public Integer getSumOfPurchases() {
        return sumOfPurchases;
    }

    public void setSumOfPurchases(Integer sumOfPurchases) {
        this.sumOfPurchases = sumOfPurchases;
    }

    public Integer getCountOfPurchases() {
        return countOfPurchases;
    }

    public void setCountOfPurchases(Integer countOfPurchases) {
        this.countOfPurchases = countOfPurchases;
    }
}
