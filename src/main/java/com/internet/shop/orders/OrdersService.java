package com.internet.shop.orders;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class OrdersService {

    private OrdersRepository ordersRepository;

    public OrdersService(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    @Transactional(readOnly = true)
    public Set<Orders> getAllOrdersOfProductByUserInLastWeek(Integer userId, Integer productId) {
        return ordersRepository.getAllOrdersOfProductByUserInLastWeek(userId, productId);
    }

    @Transactional(readOnly = true)
    SumAndCountPurchasesDTO getSumAndCountPurchasesOfProductByDate(Integer productId, String date) {
        Integer sumOfPurchases = ordersRepository.getSumOfPurchasesProductsByDate(productId, date);
        Integer countOfPurchases = ordersRepository.getCountOfPurchasesOfProductByDate(productId, date);
        return new SumAndCountPurchasesDTO(sumOfPurchases, countOfPurchases);
    }

    Set<Orders> getAllPurchasesForPeriodGroupByUserRegion(String fromDate, String toDate) {
        return ordersRepository.getAllPurchasesForPeriodGroupByUserRegion(fromDate, toDate);
    }
}
