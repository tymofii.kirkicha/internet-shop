package com.internet.shop.orders;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Integer> {

    @Query(value = "SELECT *" +
            "FROM `orders` AS o\n" +
            "WHERE o.user_id = ?1 \n" +
            "\tAND\n" +
            "o.product_id = ?2 \n" +
            "\tAND\n" +
            "o.order_date_time BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW();",
            nativeQuery = true
    )
    Set<Orders> getAllOrdersOfProductByUserInLastWeek(Integer userId, Integer productId);

    @Query(value = "SELECT SUM(o.quantity)\n" +
            "FROM orders AS o\n" +
            "WHERE\n" +
            "o.product_id = ?1\n" +
            "AND\n" +
            "DATE(o.order_date_time) = DATE(?2);",
            nativeQuery = true
    )
    Integer getSumOfPurchasesProductsByDate(Integer productId, String date);

    @Query(value = "SELECT COUNT(o.quantity)\n" +
            "FROM orders AS o\n" +
            "WHERE\n" +
            "o.product_id = ?1\n" +
            "AND\n" +
            "DATE(o.order_date_time) = DATE(?2);",
            nativeQuery = true
    )
    Integer getCountOfPurchasesOfProductByDate(Integer productId, String date);

    @Query(value = "SELECT o.id, o.order_date_time, o.quantity, o.product_id, o.user_id\n" +
            "FROM orders AS o\n" +
            "INNER JOIN country AS c ON o.user_id = c.user_id\n" +
            "WHERE\n" +
            "DATE(o.order_date_time) BETWEEN DATE(?1) AND DATE(?2)\n" +
            "GROUP BY c.title;",
            nativeQuery = true
    )
    Set<Orders> getAllPurchasesForPeriodGroupByUserRegion(String fromDate, String toDate);
}
