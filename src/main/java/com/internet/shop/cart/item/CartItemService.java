package com.internet.shop.cart.item;

import com.internet.shop.product.Product;
import com.internet.shop.product.ProductRepository;
import com.internet.shop.user.User;
import com.internet.shop.user.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class CartItemService {

    private CartItemRepository cartItemRepository;
    private UserRepository userRepository;
    private ProductRepository productRepository;

    public CartItemService(CartItemRepository cartItemRepository, UserRepository userRepository, ProductRepository productRepository) {
        this.cartItemRepository = cartItemRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
    }

    @Transactional
    public CartItem addProductToCart(Integer userId, Integer productId, Integer count) {
        User user;
        Optional<User> foundUser = userRepository.findById(userId);
        if (foundUser.isPresent()) {
            user = foundUser.get();
        } else {
            return null;
        }
        Product product;
        Optional<Product> foundProduct = productRepository.findById(productId);
        if (foundProduct.isPresent()) {
            product = foundProduct.get();
        } else {
            return null;
        }

        CartItem cartItem;
        if (count == null) {
            cartItem = new CartItem(user, product);
        } else {
            cartItem = new CartItem(user, product, count);
        }
        return cartItemRepository.save(cartItem);
    }
}
