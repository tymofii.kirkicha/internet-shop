package com.internet.shop.cart.item;

import com.internet.shop.product.Product;
import com.internet.shop.user.User;

import javax.persistence.*;

@Entity
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    private Integer count = 1;

    public CartItem(User user, Product product, Integer count) {
        this.user = user;
        this.product = product;
        this.count = count;
    }

    public CartItem(User user, Product product) {
        this.user = user;
        this.product = product;
    }

    public CartItem(Product product, int count) {
        this.product = product;
        this.count = count;
    }

    CartItem() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
