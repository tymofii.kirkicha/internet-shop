package com.internet.shop.cart.item;

import com.internet.shop.product.ProductDTO;

public class CartItemDTO {
    private Integer id;
    private ProductDTO product;
    private Integer count = 1;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
