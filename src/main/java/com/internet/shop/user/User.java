package com.internet.shop.user;

import com.internet.shop.cart.item.CartItem;
import com.internet.shop.country.Country;
import com.internet.shop.credit.card.CreditCard;
import com.internet.shop.orders.Orders;
import com.internet.shop.product.Product;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min = 3, max = 30)
    private String userName;

    @NotNull
    @Column(unique = true)
    private String email;

    @CreationTimestamp
    private LocalDateTime registrationDate = LocalDateTime.now();

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "user")
    private Country country;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private Set<CreditCard> creditCards = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private Set<CartItem> cartItems = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
    private Set<Orders> orderList = new HashSet<>();

    public User(@NotNull @Size(min = 3, max = 30) String userName, @NotNull String email) {
        this.userName = userName;
        this.email = email;
    }

    public User(@NotNull @Size(min = 3, max = 30) String userName, @NotNull String email, Country country) {
        country.setUser(this);
        this.userName = userName;
        this.email = email;
        this.country = country;
    }

    public User(@NotNull @Size(min = 3, max = 30) String userName, @NotNull String email, Country country, CreditCard creditCard) {
        country.setUser(this);
        creditCard.setUser(this);
        HashSet<CreditCard> userCreditCards = new HashSet<>();
        userCreditCards.add(creditCard);

        this.userName = userName;
        this.email = email;
        this.country = country;
        this.creditCards = userCreditCards;

    }

    public User() {
    }

    public User(String userName, String email, CartItem testCartItem, Product testProduct) {
        testCartItem.setProduct(testProduct);
        testCartItem.setUser(this);
        HashSet<CartItem> cartItemList = new HashSet<>();
        cartItemList.add(testCartItem);
        testProduct.setCartItems(cartItemList);

        this.userName = userName;
        this.email = email;
        this.cartItems = cartItemList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Set<CreditCard> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(Set<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }

    public Set<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Set<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public Set<Orders> getOrderList() {
        return orderList;
    }

    public void setOrderList(Set<Orders> orderList) {
        this.orderList = orderList;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", registrationDate=" + registrationDate +
                ", country=" + country +
                ", creditCards=" + creditCards +
                ", cartItems=" + cartItems +
                ", orderList=" + orderList;
    }
}
