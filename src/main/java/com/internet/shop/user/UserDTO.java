package com.internet.shop.user;

import com.internet.shop.cart.item.CartItemDTO;
import com.internet.shop.country.CountryDTO;
import com.internet.shop.credit.card.CreditCardDTO;
import com.internet.shop.orders.OrdersDTO;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class UserDTO {
    private Integer id;
    private String userName;
    private String email;
    private LocalDateTime registrationDate;
    private CountryDTO country;
    private Set<CreditCardDTO> creditCards = new HashSet<>();
    private Set<CartItemDTO> cartItems = new HashSet<>();
    private Set<OrdersDTO> orderList = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public CountryDTO getCountry() {
        return country;
    }

    public void setCountry(CountryDTO country) {
        this.country = country;
    }

    public Set<CreditCardDTO> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(Set<CreditCardDTO> creditCards) {
        this.creditCards = creditCards;
    }

    public Set<CartItemDTO> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Set<CartItemDTO> cartItems) {
        this.cartItems = cartItems;
    }

    public Set<OrdersDTO> getOrderList() {
        return orderList;
    }

    public void setOrderList(Set<OrdersDTO> orderList) {
        this.orderList = orderList;
    }
}
