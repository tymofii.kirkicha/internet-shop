package com.internet.shop.user;

import com.internet.shop.cart.item.CartItem;
import com.internet.shop.cart.item.CartItemRepository;
import com.internet.shop.country.Country;
import com.internet.shop.country.CountryRepository;
import com.internet.shop.orders.Orders;
import com.internet.shop.orders.OrdersRepository;
import com.internet.shop.product.Product;
import com.internet.shop.product.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

@Service
public class UserService {
    private UserRepository userRepository;
    private ProductRepository productRepository;
    private OrdersRepository ordersRepository;
    private CountryRepository countryRepository;
    private CartItemRepository cartItemRepository;

    public UserService(UserRepository userRepository, ProductRepository productRepository, OrdersRepository ordersRepository, CountryRepository countryRepository, CartItemRepository cartItemRepository) {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.ordersRepository = ordersRepository;
        this.countryRepository = countryRepository;
        this.cartItemRepository = cartItemRepository;
    }

    User create(String userName, String email, String country) {
        Country savedCountry;
        User savedUser;

        savedUser = userRepository.save(new User(userName, email));
        savedCountry = countryRepository.save(new Country(country, savedUser));

        savedCountry.setUser(savedUser);
        savedUser.setCountry(savedCountry);

        countryRepository.save(savedCountry);
        return userRepository.save(savedUser);
    }

    @Transactional(readOnly = true)
    User findById(Integer id) {
        Optional<User> optionalUser = userRepository.findById(id);
        User user;
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        } else {
            return null;
        }
        return user;
    }

    @Transactional
    Set<Orders> buyAllItemsFromCart(Integer userId) {
        User user;
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        } else {
            // User not exist
            return null;
        }
        Set<CartItem> cartItems = user.getCartItems();

        Product foundProduct;
        for (CartItem item : cartItems) {
            Optional<Product> optionalFoundProduct = productRepository.findById(item.getProduct().getId());
            if (optionalFoundProduct.isPresent()) {
                foundProduct = optionalFoundProduct.get();

                if (foundProduct.getQuantityInStock() >= item.getCount()) {
                    // Delete from basket
                    cartItemRepository.delete(item);

                    // Calculate final number of products in stock
                    Integer restQuantityInStock = foundProduct.getQuantityInStock() - item.getCount();
                    foundProduct.setQuantityInStock(restQuantityInStock);

                    // Update product in stock
                    productRepository.save(foundProduct);

                    // Create new order
                    Orders newOrder = new Orders(item.getCount(), user, foundProduct);
                    ordersRepository.save(newOrder);
                } else {
                    System.err.println("Number of products is less than number of orders required!\n" +
                            "Product title: " + foundProduct.getTitle() + "\n" +
                            "Available products in stock: " + foundProduct.getQuantityInStock());
                }
            } else {
                System.err.println("Product was deleted!");
            }
        }

        Optional<User> updatedUserOptional = userRepository.findById(userId);
        User updatedUser;
        if (updatedUserOptional.isPresent()) {
            updatedUser = updatedUserOptional.get();
        } else {
            return null;
        }
        return updatedUser.getOrderList();
    }
}
