package com.internet.shop.user;

import com.internet.shop.cart.item.CartItem;
import com.internet.shop.cart.item.CartItemDTO;
import com.internet.shop.cart.item.CartItemService;
import com.internet.shop.credit.card.CreditCard;
import com.internet.shop.credit.card.CreditCardDTO;
import com.internet.shop.credit.card.CreditCardService;
import com.internet.shop.orders.Orders;
import com.internet.shop.orders.OrdersDTO;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {
    private UserService userService;
    private CreditCardService creditCardService;
    private CartItemService cartItemService;
    private final ModelMapper modelMapper;

    UserController(UserService userService, CreditCardService creditCardService, CartItemService cartItemService, ModelMapper modelMapper) {
        this.userService = userService;
        this.creditCardService = creditCardService;
        this.cartItemService = cartItemService;
        this.modelMapper = modelMapper;
    }

    @PostMapping(value = "/create")
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserDTO create(@RequestParam String userName,
                          @RequestParam String email,
                          @RequestParam String country) {
        User user = userService.create(userName, email, country);
        return modelMapper.map(user, UserDTO.class);
    }

    @PostMapping(value = "/{userId}/add-credit-card")
    @ResponseStatus(code = HttpStatus.CREATED)
    public CreditCardDTO addCreditCard(@PathVariable Integer userId,
                                       @RequestParam Long primaryAccountNumber) {
        CreditCard creditCard = creditCardService.createCreditCard(userId, primaryAccountNumber);
        return modelMapper.map(creditCard, CreditCardDTO.class);
    }

    @GetMapping(value = "/{userId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity findById(@PathVariable Integer userId) {
        User user = userService.findById(userId);
        UserDTO userDTO;
        if (user == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            userDTO = modelMapper.map(user, UserDTO.class);
        }
        return ResponseEntity.ok(userDTO);
    }

    @PostMapping(value = "/{userId}/cart/add-product/{productId}")
    public ResponseEntity addProductToCart(@PathVariable Integer userId,
                                           @PathVariable Integer productId,
                                           @RequestParam(required = false) Integer count) {
        CartItem cartItem = cartItemService.addProductToCart(userId, productId, count);
        CartItemDTO cartItemDTO;
        if (cartItem == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            cartItemDTO = modelMapper.map(cartItem, CartItemDTO.class);
        }
        return ResponseEntity.ok(cartItemDTO);
    }

    @PostMapping(value = "/{userId}/cart/buy-all-items")
    public Set<OrdersDTO> buyAllItemsFromBasket(@PathVariable Integer userId) {
        Set<Orders> ordersSet = userService.buyAllItemsFromCart(userId);
        if (ordersSet.size() > 1) {
            return ordersSet.stream()
                    .map(order -> modelMapper.map(order, OrdersDTO.class))
                    .collect(Collectors.toSet());
        } else {
            return null;
        }
    }
}
