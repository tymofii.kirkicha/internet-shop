DELETE
FROM user
WHERE user.id = 0;
DELETE
FROM product
WHERE product.id = 0;
DELETE
FROM cart_item
WHERE cart_item.user_id = 0;
DELETE
FROM orders
WHERE orders.user_id = 0;